//Define usage
if (process.argv.length < 3) {
  console.log('Usage: node ' + process.argv[1] + ' FILENAME');
  process.exit(1);
}

//Define file name
var fs = require('fs')
  , filename = process.argv[2];

//Read data from file and begin parse and search process
fs.readFile(filename, 'utf8', function(err, data) {
  if (err) throw err;
  parseAndSearch(data);
});

function parseAndSearch(data){
  inputArr = data.split('\n'); //Split data into array by return character

  //Remove special characters and spaces
  for(let itm in inputArr){
    inputArr[itm] = inputArr[itm].replace(/[^a-z0-9]/gi,'');
  }

  //Remove empty elements, if any
  for(let itm in inputArr){
    if(inputArr[itm] === ""){
      inputArr.splice(itm,1);
    }
  }

  //Determine the size of letter array
  soupY = parseInt(inputArr[0].split('x')[1]);

  //Split data into letter array and word list
  soup = inputArr.slice(1,soupY+1);
  wordList = inputArr.slice(soupY+1);

  //Search letter array for each word
  for(let word of wordList){
    console.log(word + " " + soupSearch(soup, word));
  }
}

//Search by checking each letter in array for
//first letter of the word and searching in 
//each direction
function soupSearch(soup, word){
  const dirs = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]; //Array of 8 directions
  //Itterate though array and find first letter
  for(let y in soup){
    for(let x in soup[y]){
      if(word[0] == soup[y][x]){
        //Fount first letter, search all 8 directions
        for(let dir of dirs){
          let wordFound = searchDir(dir,x,y,word); //Search in this direction
          if(wordFound){
            return (y+ ":" + x + " " + wordFound); //Return output string
          }
        }
      }
    }
  }
}

//Recursive function to search for the word in a specific direction
function searchDir(dir,x,y,word,i=1){
  //Itteration length matches word length, word found!
  if(i==word.length){
    return (x + ":" + y);
  }

  //Step in specified direction
  xd = dir[0]+parseInt(x);
  yd = dir[1]+parseInt(y);

  //Return if step is out of bounds
  if(xd<0 || xd>=soup[y].length || yd<0 || yd >= soup.length){
    return;
  }

  //Restart function of letter is found
  if(soup[yd][xd] == word[i]){
    return searchDir(dir,xd,yd,word,i+1);
  }
}